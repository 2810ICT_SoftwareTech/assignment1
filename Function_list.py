import re
import sys

##USER FUNCS##

def fileExists(path):
    try:
        file = open(path)     # checks to see if it can open said file,
        file.close()          # and then closes it as required.
    except FileNotFoundError: # if an error is recieved trying to open the file then the following will happen
        return False          # if the file could not be found then a false flag is returned.
    return True               # otherwise if true, the file was found and the code can continue on working
#-
def getDictName(name):
    while True:
        fname = name
        if ".txt" not in fname:     # If the user omits the .txt,
            fname += ".txt"         # it is appended the the end of the input.
        if fileExists(fname):       # Check the provided file name to see if it exists
            return(fname)           # Returns the name of the dictionary name the user has requested.
        else:                       # Otherwise ask the user for another name.
            return(0)
#-
def skipWords(start, target, listIn, removeWords):
    reWords = removeWords
    for word in reWords:                                                  # For each word supplided by the user,  
        if word.lower() in listIn and (word != start or word != target):  # check to make sure word isn't start or target word,
            listIn.remove(word.lower())                                   # remove word from list,
    return(listIn)

##EXISTING FUNCS##

def same(item, target):  # Same compares every character of item against target
    return len([c for (c, t) in zip(item, target) if c == t])  # Returns true if every character in item, matches target
#-
def build(pattern, words, seen, list):
    return [word for word in words
            if re.search(pattern, word)  # searches the current word itteration matches the pattern
            and word not in seen.keys()  # checks the word is not a key of the Seen dictionary
            and word not in list]  # checks if the word is already within the generated list
#-
def find(word, words, seen, target, path):
    list = []  # declares a  list
    for i in range(len(word)):  # iterates though range based on the length of the start word
        if word[i] == target[i]:  # checks if the letters already matches with the target word
            continue  # Runs though the word varible itterating though each letter
        list += build(word[:i] + "." + word[i + 1:], words, seen, list)  # calls build function to generate list based on iterated widlcard char
        #DEBUG print(list)  # Calls the build function to construct a list
    if len(list) == 0:
        return False  # If the list is empty return false as there are no matches
    list = sorted([(same(w, target), w) for w in list], reverse=True)  # sorts the list in reverse
    #DEBUG print(list)  # Sorts the list based on the value returned by same assosiated to the W key
    for (match, item) in list:  # iterates though the list
        if match >= len(target) - 1:  # checks if the length of match is atleast the same or greater length than target
            if match == len(target) - 1:  # checks if match is smaller than the length of target by one
                path.append(item)  # appends item to path list
            return True  # returns true if match is the same as target or one letter away
        seen[item] = True  # adds item as a key to seen
    for (match, item) in list:  # runs though list
        path.append(item)  # appends items to path
        if find(item, words, seen, target, path):  # calls the function find and uses the iterator as the new start
            return True  # return true if the if find statment if true
        path.pop()  # removes the last item of path list