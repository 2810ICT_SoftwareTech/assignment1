import unittest
import re

from Function_list import fileExists
from Function_list import getDictName
from Function_list import skipWords

from Function_list import same
from Function_list import build
from Function_list import find

class TestCase(unittest.TestCase):
""" 
    def test_fileExists(self):
        filenames = ['floor.txt', 'store.txt', 'Harry Potter and the Goblet of Alchohol.txt']
        rightname = ['dictionary.txt']

        for i in range(len(filenames)):
            self.assertNotEqual(fileExists(filenames[i]), True)
        self.assertEqual(fileExists(rightname), True)

    def test_getDictName(self):
        filenames = ['floor', 'store', 'Harry Potter and the Goblet of Alchohol.txt']
        rightname = ['dictionary', 'dictionary.txt']

        for i in range(len(filenames)):
            self.assertEqual(getDictName(filename[i]),0)
        for i in range(len(rightname)):
            self.assertEqual(getDictName(rightname[i]),'dictionary.txt') 

    def test_skipWords(self):
        listProvided = ['ball', 'haze', 'legs', 'feel', 'snorkle', 'kits', 'seen', 'been', 'sean', 'bean']
        removeWords = ['ball', 'haze', 'legs', 'seen', 'been']
        remainWord = ['ball', 'haze', 'feel', 'snorkle', 'kits', 'sean', 'bean']
        keepWords = ['ball','haze']

        self.assertEqual(skipWords(keepWords[0],keepWords[1],listProvided, removeWords), remainWords)
"""
    def test_same(self):
        words = ['glue', '1234', 'fo od', 'FOOD', '', 'foo']
        largerthan = 'fooded'
        
        for i in range(len(words)):
            self.assertNotEqual(same('food', words[i]), 4)
        self.assertEqual(same('food', largerthan), 4)

    def test_build(self):
        word = 'lead'
        words = ['bead', 'dead', 'head', 'mead', 'read', 'load', 'lend', 'leud', 'lewd', 'ford', '1234', 'seed', 'chad']
        word_noncomply = ['ford', '1234', 'seed', 'chad', 'bead', 'ford', '1234', 'seed', 'chad']
        seen = {'bead': True}
        list1 = []
        
        for i in range(len(word)):
            list1 += build(word[:i] + "." + word[i + 1:], words, seen, list1)
        for j in range(len(list1)):
            self.assertNotIn(list1[j], word_noncomply)
            print(list1)

    def test_find(self):
        file = open('dictionary.txt', 'r')
        lines = file.readlines()
        word_list = []
        seen = {}
        path = []
        start = 'lead'
        
        for line in lines:  #
            word = line.rstrip()  # rstrip() clears white space
            if len(word) == len(start):  # adds only words who share a length with start to dict
                word_list.append(word)
        file.close()
        self.assertTrue(find('lead', word_list, seen, 'gold', path))
        self.assertTrue(find('hide', word_list, seen, 'seek', path))
        self.assertFalse(find('', word_list, seen, 'gold', path))

if __name__ == "__main__":
    unittest.main()