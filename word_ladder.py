import re
import sys

## FN Checks to see if file exists ##
def fileExists(path):
    try:
        file = open(path)     # checks to see if it can open said file,
        file.close()          # and then closes it as required.
    except FileNotFoundError: # if an error is recieved trying to open the file then the following will happen
        return False          # if the file could not be found then a false flag is returned.
    return True               # otherwise if true, the file was found and the code can continue on working

## FN getDictName - asks user for a dictonary name to open ##
def getDictName():
    while True:
        fname = input("Enter dictionary name: ")
        if ".txt" not in fname:     # If the user omits the .txt,
            fname += ".txt"         # it is appended the the end of the input.
        if fileExists(fname):       # Check the provided file name to see if it exists
            return(fname)           # Returns the name of the dictionary name the user has requested.
        else:                       # Otherwise ask the user for another name.
            print("Could not find a file called "+fname+", please enter another file name.\n")

## FN skipWords - asks user for words to exclude when searching for a path ##
def skipWords(start, target, listIn):
    reWords = str(input("Please list the words you wish to exclude, seperated by spaces.\n")).split()
    for word in reWords:                                                  # For each word supplided by the user,  
        if word.lower() in listIn and (word != start or word != target):  # check to make sure word isn't start or target word,
            listIn.remove(word.lower())                                   # remove word from list,
    return(listIn)                                                        # return modified list.

## Existing Functions Below ##

## FN compares every character of item against target  ##
## eg. comparing lead to gold will iterate through     ## 
## l==g, e==o, a==l and d==d, if all of these are true ##
## then the provided word is the same as the target    ##
def same(item, target):                                     
    return len([c for (c, t) in zip(item, target) if c == t]) # Returns true if every character in item, matches target


## Using the provided inputs, build creates a list of words that are a ##
## single character away from the current word using a wildcard value  ##
## eg. lead is treated as .ead and the list will look similar to below ##
##              ['head', 'read', 'bead', 'mead', 'dead']               ##
def build(pattern, words, seen, list):
    return [word for word in words           # Returns a word word to be added to the list                                              
                 if re.search(pattern, word) # searches the current word iteration matches the pattern 
                 and word not in seen.keys() # checks the word is not a key of the 'seen' dictionary
                 and word not in list]       # checks if the word is already within the generated list

## Recursively iterates through the dictionary searching ##
## for a path between the start and target word.         ##
## Each iteration selects a word that is closest to the  ##
## target word and proceeds to repeat this until it has  ##
## reached the target word. It then provides a list to   ##
## of the words that it used to reach the target.        ##
def find(word, words, seen, target, path):
    list = []
    for i in range(len(word)):                                         # Iterates through range based on the length of the start word
        if word[i] == target[i]:                                       # Checks if the letters already matches with the target word
            continue                                                   # Runs through the word varible itterating through each letter
        list += build(word[:i] + "." + word[i + 1:], words, seen,list) # Calls build function to generate list based on iterated wildcard char
                                                                  
    if len(list) == 0:
        return False                                                   # If the list is empty return false as there are no matches
    list = sorted([(same(w, target), w) for w in list], reverse=True)  # Sorts the list based on the value returned by same assosiated to the 'w' key
                                                                       
    for (match, item) in list:                                         # Iterates through the list
        if match >= len(target) - 1:                                   # Checks if the length of match is atleast the same or greater length than target
            if match == len(target) - 1:                               # Checks if match is smaller than the length of target by one
                path.append(item)                                      # Appends item to path list
            return True                                                # Returns true if match is the same as target or one letter away
        seen[item] = True                                              # Adds item as a key to seen

    for (match, item) in list:                                         # Iterates through the list
        path.append(item)                                              # Appends items to path
        if find(item, words, seen, target, path):                      # Calls the function find and uses the iterator as the new start
            return True                                                # Return true if the if find statment if true
        path.pop()                                                     # Removes the last item of path list
 
##  CODE DEALING WITH USER INPUTS  ##

# See if dictionary.txt exists, if it does, ask user if they want to use it or use another dictionary.
if fileExists("dictionary.txt"):
    while True:
        result = str(input("Found file called \"dictionary.txt\", would you like to use it? (Y/N): ")).lower()
        if result == "n" or result == "no":
            fname = getDictName()                 # Asks user for dictionary name
            break
        elif result == "y" or result == "yes" or result == "":
            fname = "dictionary.txt"              # Defaults to standard dictionary name
            break
        else:
            print("Unsupported character, Please enter Y or N.\n")
else:   # If no default dictionary exists, ask user for a dictionary name
    fname = getDictName()

# Reads lines from the provided dictionary and asks user for a start and target word
file = open(fname)
lines = file.readlines()
file.close()

foundStart = False                                     
while foundStart == False:
    start = input("Enter start word:").lower().strip()       # Asks the user for an input
    for line in lines:                                       # Breaks down the dictionary into lines       
        word = line.rstrip()                                 # and those lines into individual words
        if word == start:                                    # if the entered word is found in dictionary
            foundStart = True                                # Set the loop breaker condition to true
            break                                            # break out of the loop to save precious computing power
    if foundStart == False:                                  # Check to see if the word entered exists in dict
        print("Cannot find \""+start+"\" in "+fname+".\n")   # If it isn't found, ask the user for another input
  
words = []
for line in lines:              # iterates through every line in the provided dictionary
    word = line.rstrip()        # rstrip() clears white space
    if len(word) == len(start): # adds only words who share a length with start to dict
        words.append(word)      # 
  
while True:
    target = input("Enter target word:").lower().strip()                  # Asks the user for an input that the program will step towards
    if target == start:                                                   # If the target word is the same as the start word,
        print("Your target cannot be the same word as the start word.\n") # the user is informed that they cannot do that.
    elif len(target) != len(start):                                       # Check to see if target has less or more letters than start,
        print("\""+target+"\" isn't the same length as \""+start+"\".\n") # and informs the user if their lengths don't match.
    elif target not in words:                                             # Checks to see if target word is in dictionary,
        print("\""+target+"\" couldn't be found in "+fname+".\n")         # and informs the user to select a differnt word if it's not there.
    else:                                                                 # If the provided word is valid,
        break                                                             # then the loop is broken and the program can continue.

while True:   
    skipable = str(input("Are there any words you wish to exclude from being used? (Y/N): ")).lower()
    if skipable == "n" or skipable == "no":                       # The program will also accept commonly used responses.
        break
    elif skipable == "y" or skipable == "yes" or skipable == "":  # The program will also accept commonly used responses.
        words = skipWords(start, target, words)                   # The user will then be prompted to enter in words to exclude.
        break
    else:
        print("Unsupported character, Please enter Y or N.\n")    # If the entered string did not comply, the user will be asked for another input.

print("\nSearching "+fname+" for connection between \""+start+"\" and \""+target+"\".\n")

##  END OF CODE DEALING WITH USER INPUTS  ##

count = 0
path = [start]
seen = {start : True}
if find(start, words, seen, target, path):
    path.append(target)
    print(len(path) - 1, path)
else:
    print("No path found")
